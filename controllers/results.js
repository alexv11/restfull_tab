const express = require('express');

function sumar(req, res, next) {
    const a = parseInt(req.params.a);
    const b = parseInt(req.params.b);
    console.log(`sum parameters => ${a} + ${b} = ${a + b}`);
    res.send(`sum parameters => ${a} + ${b} = ${a + b}`);
}

function multiplicar(req, res, next) {
    const a = parseInt(req.body.a);
    const b = parseInt(req.body.b);
    res.send(`multiply parameters => ${a} * ${b} = ${a * b}`);
}

function dividir(req, res, next) {
    const a = parseInt(req.body.a);
    const b = parseInt(req.body.b);
    res.send(`divide parameters => ${a} / ${b} = ${a / b}`);
}

function potencia(req, res, next) {
    const a = parseInt(req.body.a);
    const b = parseInt(req.body.b);
    res.send(`power parameters => ${a} ^ ${b} = ${Math.pow(a, b)}`);
}

function restar(req, res, next) {
    const a = parseInt(req.params.a);
    const b = parseInt(req.params.b);
    res.send(`rest parameters => ${a} - ${b} = ${a - b}`);
}

module.exports = { sumar, multiplicar, dividir, potencia, restar };