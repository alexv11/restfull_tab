var express = require('express');
const { sumar, multiplicar, dividir, potencia, restar } = require('../controllers/results');
var router = express.Router();

router.get('/:a/:b', sumar);
router.post('/', multiplicar);
router.put('/', dividir);
router.patch('/', potencia);
router.delete('/:a/:b', restar);

module.exports = router;
